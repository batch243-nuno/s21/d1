// console.log("Hello World");

// Array

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// With Array
let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

 let grades = [98.5, 94.3, 89.2, 90.1];
 console.log(grades);

 let computerBrands = ["Acer", "ASUS", "Lenovo", "Dell", "Mac", "Samsung"];
 console.log(computerBrands);

// mixed data type array is no recommended
 let mixedArr = [12, "Asus", null , undefined, {}];
 console.log(mixedArr);

//  alternative way to write arrays
// let myTasks = [
//     "drink html", 
//     "eat javascript", 
//     "inhale css", 
//     "bake sass"];
// console.log(myTasks);

// array with value from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];
console.log(cities);

// length property
console.log(grades.length);
console.log(cities.length);

let blankedArr = [];
console.log(blankedArr.length);
// deleting
let myTasks = [
    "drink html", 
    "eat javascript", 
    "inhale css", 
    "bake sass"];
console.log(myTasks);
myTasks.length = myTasks.length-1;
console.log(myTasks);

// to delete specific item
console.log(cities);
cities.length--;
console.log(cities);

// adding
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length += 1;
console.log(theBeatles);

theBeatles[4] = "Roda";
console.log(theBeatles);

// Reading / Accesion elements of arrays
let lakersLegend = ["Kobe", "Shaq", "Lebro", "Magic", "Kareem"];
console.log(lakersLegend[1]);
console.log(lakersLegend[4]);

let currentLaker = lakersLegend[2];
console.log(currentLaker);

// reassign value
console.log(`Array before reassignment:`);
console.log(lakersLegend);

console.log(`Array after reassignment:`);
lakersLegend[2] = "Pau Gasol";
console.log(lakersLegend);

// Accessing the last element of array
let lastElementIndex = lakersLegend.length-1
console.log(lakersLegend[lastElementIndex]);

// add with out using method
let newArr = [];
newArr[0] = "Cloud Strife";
console.log(newArr);
newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);
newArr[newArr.length] = "Barret";
console.log(newArr);
newArr[newArr.length] = "Chris";

// looping over an array
for (let index = 0; index < newArr.length; index++) {
    console.log(`${newArr[index]}`);
}

let numArr = [5, 12, 30, 46, 40];
for (let index = 0; index < numArr.length; index++) {
    if (numArr[index] % 5 === 0) {
        console.log(`${numArr[index]} is divisible by 5.`);
    }
    else{
        console.log(`${numArr[index]} is not divisible by 5.`)
    }
}

// Multi-Dimensional Array
let chessBoard = [
    ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1','H1'],
    ['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2','H2'],
    ['A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3','H3'],
    ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4','H4'],
    ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5','H5'],
    ['A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6','H6'],
    ['A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7','H7']
];
console.table(chessBoard);
console.table(chessBoard[3][5]);
 

